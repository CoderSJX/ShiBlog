### **[96. 不同的二叉搜索树](https://leetcode-cn.com/problems/unique-binary-search-trees/)**

难度中等1362

给你一个整数 `n` ，求恰由 `n` 个节点组成且节点值从 `1` 到 `n` 互不相同的 **二叉搜索树** 有多少种？返回满足题意的二叉搜索树的种数。

解题：

1. 不同的二叉搜索树和具体节点的值没有关系。
2. 与节点的数量有关系，节点数量决定了二叉树的种数。
3. 所以某个二叉搜索树的种数=左子树的二叉搜索树的个数*右子树的二叉树的个数
4. 因此把这个二叉树分成两部分

```java
package tree;

public class NumTrees {
    public int numTrees(int n) {
        int dp[]=new int[n+1];
        dp[1]=1;
        dp[0]=1;
        for (int i = 2; i <= n; i++) {
            for (int j = 1; j<= n; j++) {
                dp[i]+=dp[i-j]*dp[j-1];
            }
        }

        return dp[n];
    }
}
```